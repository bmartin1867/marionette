#!/usr/bin/env python

# Marionette
# Guided Interface for ANN Processing.

# Thomas K Ales, Iowa State University.
# tkales [at] iastate [dot] edu
# Version 2 rewrite

# Imports
import os
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

verbose = True

# File System Configuration
# workingDirectory should be the absolute path to where the 
#                   script can copy in new instances of the 
#                   network files.

workingDirectory = '/home/zorak/Documents/'

# cleanPath is the absolute path to where the script can get 
#           a fresh copy of the network

cleanPath = '/opt/blankANN/'

# runName is the default name of the root folder
runName = 'v2Test'

instancePath = workingDirectory + runName + '/'

# Training/Testing Ratio
trainingRatio = 0.70

# topNetworks is the number of networks you want to the best errors for
topNetworks = 2

def replaceSpec(specPath, inputNums, trainingSize, totalSize):

    specPath = specPath+'src/spec.t1'
    with open(specPath, 'r') as specFileHandle:
        specFile = list(filter(None, specFileHandle.read().splitlines()))
        specFileHandle.close()
    # Change the total number of records to use
    # Modify the input layer to match the number of inputs
    specFile[3] = specFile[3].replace('3', str(inputNums), 1)
    specFile[18] = specFile[18].replace('75', str(totalSize), 1)
    specFile[19] = specFile[19].replace('50', str(trainingSize), 1)
    testSet_string = str(trainingSize) + '\t' + str(totalSize)
    specFile[20] = specFile[20].replace('51      75', testSet_string, 1)
    specFile[21] = specFile[21].replace('51\t75', testSet_string, 1)
    print('FILEOP: Modified spec.t1 file, writing out.')
    specFileHandle = open(specPath, 'w')
    for currentLine in specFile:
        specFileHandle.write(currentLine+'\n')
    specFileHandle.close()
    print('FILEOP: spec.t1 written to '+specPath)
    return specPath
# 
def copyNewNetwork(destinationFolder):
        try:
            os.stat(destinationFolder)
            pass
        except FileNotFoundError:
            os.system('mkdir '+destinationFolder)

        os.chdir(workingDirectory)
        os.system('cp -r '+cleanPath+'/* '+destinationFolder)
        os.system('chmod -R 775 '+destinationFolder)
        if verbose:
            print('INFO: Copying clean ANN from '+cleanPath)
            print('INFO: to '+destinationFolder)
            print('INFO: Changed permissions to 775')

try:
    os.stat(instancePath)
    print('WARNING: There already exists a folder at this location.')
    clearFolder = input('WARNING: Delete folder '+runName+' [y]/n?: ')
    if clearFolder == ('' or 'y'):
        os.chdir(workingDirectory)
        os.system('rm -r ./'+runName)
        if verbose:
            print('INFO: Deleted '+runName)

        copyNewNetwork(instancePath)
except: 
        copyNewNetwork(instancePath)

def editCommittee(numOfInputs, committeePath=instancePath):
    with open(committeePath+'in/committee.dat') as committeeFile:
        committeeStr = committeeFile.read()
        committeeConfig = committeeStr.split(' ')
        if committeeConfig[1] != numOfInputs:
            committeeConfig[1] = str(numOfInputs)
            committeeConfig.append('\n')
        committeeFile.close()

    with open(committeePath+'in/committee.dat', 'w') as committeeFile:
        committeeFile.write(' '.join(committeeConfig)) 
        committeeFile.close()

# Get file information
inputFilePath = input('Provide the path to the tagged CSV file:')
delimiterType = input('How is the file delimited? [Default = ,]:')
headerLines = input('How many header lines are there before the '
                    +'x_ y_ tags start? [Default = 0]:')
if delimiterType == '':
    delimiterType = ','

if headerLines == '':
    headerLines = 0

# Open the input CSV file
try:
    df_inputFile = pd.read_csv(inputFilePath, sep=delimiterType, 
                                skiprows=int(headerLines))
except FileNotFoundError:
    print('ERROR: File wasn\'t found.')
    inputFilePath = input('Provide the path to the tagged CSV file:')
    df_inputFile = pd.read_csv(inputFilePath, sep=delimiterType, 
                                skiprows=int(headerLines))

# Predefine the input and output lists
predictedInputs = []
predictedOutputs = []

# Go through each column of the dataframe, check to see if the column label
# begins with either an X or x, if it does, append that column to a list of inputs.
# For Y or y labelled columns, append that to the list of possible outputs.

for currentColumn in df_inputFile.axes[1]:
    if currentColumn.startswith('X') or currentColumn.startswith('x'):
        predictedInputs.append(df_inputFile.loc[:, currentColumn])

    if currentColumn.startswith('Y') or currentColumn.startswith('y'):
        predictedOutputs.append(df_inputFile.loc[:, currentColumn])

numOfInputs = input('CONFIRM: It looks like there are '
                    +str(predictedInputs.__len__())+' inputs.\n'
                    +'If this isn\'t what you\'re expecting, ^C out and check the input CSV.')
if numOfInputs == '':
    numOfInputs = predictedInputs.__len__()
    activeInput = pd.DataFrame(predictedInputs)
    activeInput = activeInput.T

# If there is more than one output, ask which one we should use, otherwise
# continue, using the y/Y tagged output.

if predictedOutputs.__len__() > 1:
    print('CONFIRM: It looks like there is more than one output variable.')
    print('CONFIRM: I\'m going to print the names of the column labels,'
        + ' please tell me which one is the output you want to use.')

    for index, currentOutput in enumerate(predictedOutputs):
        print('CONFIRM: ['+str(index)+'], Label: \''+currentOutput.name+'\'');

    try:
        outputIndex = int(input('CONFIRM: Which should I use as the output variable [default = 0]:'))

    except ValueError:
        outputIndex = 0

    activeOutput = predictedOutputs[outputIndex]

else:
    activeOutput = predictedOutputs[0]

print('INFO: The current training / testing ratio is set at: '+'{:.2%}'.format(trainingRatio)+
      ' / '+'{:.2%}'.format(1 - trainingRatio))

recordsForTraining = trainingRatio * activeOutput.__len__()

print('INFO: Ideal target is ' + '{:d}'.format(int(round(recordsForTraining)))
    + ' records will be used for the training set.')

# Split the data into testing and training.
testingInput = []
trainingInput = []
testingOutput = []
trainingOutput = []

# Initalize PRNG
random.seed()
print('INFO: Using internal Python Psuedo-RNG for selection. Cutoff set at '
     +'{:.2f}'.format(trainingRatio))

for recordIndex in range(0, activeInput.__len__()):
    sortRand = random.random()

    if verbose:
        print('Record index: '+str(recordIndex)+' Random: '+'{:.5f}'.format(sortRand))

    if (sortRand < trainingRatio) & (trainingInput.__len__() < recordsForTraining):

        if trainingInput.__len__() < recordsForTraining:
            trainingInput.append(activeInput.iloc[recordIndex,:])
            trainingOutput.append(activeOutput[recordIndex])

        elif (sortRand < trainingRatio) & (trainingInput.__len__() >= recordsForTraining):
            testingInput.append(activeInput.iloc[recordIndex, :])
            testingOutput.append(activeOutput[recordIndex])
    else:
        testingInput.append(activeInput.iloc[recordIndex,:])
        testingOutput.append(activeOutput[recordIndex])

print('INFO: Sort did '+str(trainingInput.__len__())+' training records, '
    + str(testingInput.__len__())+' testing records.')
print('INFO: Split Percentage is '+ '{:.1%}'.format(trainingInput.__len__()/activeOutput.__len__())
    + ' / ' + '{:.1%}'.format(testingInput.__len__()/activeOutput.__len__())
    + ' training / testing.')

# Convert the lists into dataframe objects

df_testingInput = pd.DataFrame(testingInput)
df_trainingInput = pd.DataFrame(trainingInput)
s_testingOutput = pd.Series(testingOutput)
s_trainingOutput = pd.Series(trainingOutput)

df_trainingInput.to_csv(instancePath+'in/trainingSet_inp.dat', sep=' ', index=False,
                        header=False)
df_testingInput.to_csv(instancePath+'in/testingSet_inp.dat', sep=' ', index=False,
                        header=False)
os.chdir(instancePath)
os.system('cat ./in/trainingSet_inp.dat ./in/testingSet_inp.dat > ./in/test.dat')

s_trainingOutput.to_csv(instancePath+'in/trainingSet_out.dat', sep=' ', index=False,
                        header=False)
s_testingOutput.to_csv(instancePath+'in/testingSet_out.dat', sep=' ', index=False,
                        header=False)
os.system('cat ./in/trainingSet_out.dat ./in/testingSet_out.dat > ./in/TiAlloy_data-001n_yield.dat')
os.system('cp ./in/test.dat ./')
os.system('cp ./in/test.dat ./in/TiAlloy_data-001n_input.dat')
editCommittee(numOfInputs)
os.system('./bin/no_of_lines.x ./in/test.dat ./in/no_of_rows.dat')

# Process the inputs and output
os.system('./bin/datapro -t -i'+'{:02d}'.format(numOfInputs)
            +' ./in/test.dat ./in/TiAlloy_data_001n_inputs_norm.dat')
os.system('mv ./MINMAX ./MINMAX_inputs')

os.system('./bin/datapro -t -i01 ./in/TiAlloy_data-001n_yield.dat ./in/TiAlloy_data-001n_yield_norm.dat')
os.system('mv ./MINMAX ./MINMAX_outputs')
os.system('cat ./MINMAX_inputs ./MINMAX_outputs > ./MINMAX')

# Rewrite the MINMAX file, and copy it into the ./in/ directory.
with open(instancePath+'/MINMAX', 'r') as minmaxFile:
    splitMinmax = minmaxFile.read().splitlines()

    fixedMinmax = []
    for idx, currentLine in enumerate(splitMinmax):
        if idx == numOfInputs+2:
            pass
        elif idx == numOfInputs+3:
            pass
        elif idx == numOfInputs+4:
            currentLine = currentLine.replace('1', str(numOfInputs+1), 1)
            fixedMinmax.append(currentLine)
        else:
            fixedMinmax.append(currentLine)
    minmaxFile.close()

with open(instancePath+'/MINMAX', 'w') as minmaxFile:
    for currentLine in fixedMinmax:
        minmaxFile.writelines(currentLine+'\n')
    minmaxFile.close()

os.system('mv ./MINMAX ./in/MINMAX')
os.system('./bin/normtest')

replaceSpec(instancePath, numOfInputs, df_trainingInput.__len__(),
            (df_testingInput.__len__() + df_trainingInput.__len__()))

# Determine the number of hidden nodes
print('CONFIRM: There are '+str(numOfInputs)+' inputs, currently I am going to run up to '+\
    str(numOfInputs)+' hidden layers.')

maxHiddenNodes = numOfInputs
# Default seed values
print('CONFIRM: Default seed values are ')
seedValues = [10, 100, 680, 1000, 100000, 1000000, 10000000]
# Default Sigma Values
sigmaWValues = [.01, .1, .5, 1.0, 3.0, 5.0, 10.0, 15.0, 20.0]

# Make a pretty string to print the seed values with, and repeat the process
# for the sigmaW values
seedValueString = ''
for index, currentEntry in enumerate(seedValues):
    if index == 1:
        seedValueString = str(currentEntry)
    else:
        seedValueString = seedValueString + ', ' + str(currentEntry)

sigmaValueString = ''
for index, currentEntry in enumerate(sigmaWValues):
    if index == 1:
        sigmaValueString = str(currentEntry)
    else:
        sigmaValueString = sigmaValueString + ', ' + str(currentEntry)

print('CONFIRM: ' + seedValueString)
# Ask if they want to change the seed.
changeSeed = input('CONFIRM: Press enter to accept, or enter new values:')
if changeSeed != '':
    # Chunk string into list
    seedValues = float(changeSeed.split(','))

# Do the same thing for the sigmaW values.
print('CONFIRM: Default sigmaW values are:')
print('CONFIRM: '+sigmaValueString)
changeSigma = input('CONFIRM: Press enter to accept, or enter new values:')
if changeSigma != '':
    sigmaWValues = float(changeSigma.split(','))

#######
# Neural Network Control Section
#######

# Hidden Node Loop, count from 1 to max inputs
arch = []
for currentHiddenNodes in range(1, numOfInputs+1):
#   Iterate through all the seed values
    for currentSeed in seedValues:
#       Iterate through all the sigmaW values
        for currentSigma in sigmaWValues:
#           Execute the bigback5 NN
            replaceSpec(instancePath, currentHiddenNodes, df_trainingInput.__len__(),
                            (df_testingInput.__len__() + df_trainingInput.__len__()))
            bigBackString = './bin/bigback5 '+instancePath+'src/spec.t1'\
                            +' '+str(currentSeed)+' '+str(currentHiddenNodes)\
                            +' '+str(currentSigma)+' ./r/_R ./r/_r ./w/_sujoy'
            
            os.chdir(instancePath)
            os.system(bigBackString)
            bigBackString = ''

            arch.append([currentHiddenNodes, currentSeed, currentSigma])
    # Copy the file\ns over for this given number of hidden nodes.
    os.system('cd '+instancePath+' & mv ./r/_R ./r/_R-'+str(currentHiddenNodes)+\
        '& mv ./r/_r ./r/_r-'+str(currentHiddenNodes))

# Read in the _r summary file, and append the architecture
# information to it so you can do some evaluation
# Also have to fix the header to remove the last two columns, allows appending
# the architectures to the end of the report file easier.
fixedHeaderString = '#H S_W[1]  S_W[2]  S_W[3]  S_W[4]  S_W[5]  S_W[6]  SIGMA_NU D_EN'\
    + '    D_X2 /NDF WX2/k1 WX2/k2 WX2/k3 WX2/k4 WX2/k5 WX2/k6 GAMMA3 LOGOC3  '\
    + 'LOGOCF  SYMS NU T_EN   MT_EB     T_X2   NDF  T_EN 2   MT_E2B   T_X2 2 '\
    + 'NDF  EVAB    EVB EVDENCE EVAB2 '

# Append the architecture information to the report file

for nodeCount in range(1, numOfInputs+1):
    summaryReportPath = instancePath+'r/_r-'+str(nodeCount)
    summaryReportRaw = []
    with open(summaryReportPath, 'r') as reportFile:

        for index, currentLine in enumerate(reportFile):

            if index == 0:
                summaryReportRaw.append(fixedHeaderString)

            else:

                if index % 2 != 0:
                    # Not a header line
                    summaryReportRaw.append(currentLine)

# Rewrite the summary report files into something that will be easy to import
# as a pandas dataframe. Tack on the architecture settings at the end as well.
    with open(summaryReportPath, 'w') as reportFile:
        archIndex = (seedValues.__len__() * sigmaWValues.__len__())*(nodeCount-1)

        for indexNo, currentLine in enumerate(summaryReportRaw):

            if indexNo == 0:
                reportFile.write(fixedHeaderString+' NODES  SEED  SIGMA\n')

            else:
                # the [1:-2] trims the leading space, and the newline from the
                # original report. Makes everything line up
                reportFile.write(currentLine[1:-2]+' '+str(arch[archIndex][0])+\
                ' '+str(arch[archIndex][1])+' '+str(arch[archIndex][2])+'\n')
                archIndex += 1

# Now re-import everything as a pandas dataframe:
df_results = []
smallestErrors = []
# Only import the T_EN, D_EN, SEED and SIGMA columns

print('Sorting architectures by smallest T_EN.')

for nodeCount in range(1, numOfInputs+1):
    reportPath = instancePath+'r/_r-'+str(nodeCount)
    df_results.append(pd.read_csv(reportPath, delim_whitespace=True, header=0,
        usecols=[8,22,37,38]))
    smallestErrors.append(df_results[nodeCount-1].nsmallest(topNetworks*2, 'T_EN'))
    smallestErrors[-1][r'HIDDEN'] = nodeCount

eachArchError = []
print('Picking architecture with best error for each hidden node level.')

for archMember in smallestErrors:
    eachArchError.append(archMember.nsmallest(topNetworks, 'T_EN'))
    
eachArchError = pd.concat(eachArchError)

bestArches = eachArchError
bestArches.reset_index(inplace=True)
for idx in range(0, bestArches.__len__()):
    print('Arch '+str(idx)+': # Hidden Nodes: '+str(bestArches.at[idx, 'HIDDEN'])
            +' Sigma='+str(bestArches.at[idx, 'SIGMA'])+' Seed='
            +str(bestArches.at[idx, 'SEED'])+' T_EN: '
            +str(bestArches.at[idx, 'T_EN']))

# print('Best Archecture Found was using '+str(bestError.get_value(0,'HIDDEN'))
#     + ' hidden nodes. Sigma='+str(bestError.get_value(0,'SIGMA'))
#     + ' Seed='+str(bestError.get_value(0,'SEED'))+' Training Error (T_EN):'
#     + str(bestError.get_value(0,'T_EN')))
# print('2nd Best: '+str(bestError.get_value(1,'HIDDEN'))+' hidden nodes. Sigma='
#     + str(bestError.get_value(1, 'SIGMA'))+' Seed='+str(bestError.get_value(1, 'SEED'))
#     + ' Training Error (T_EN):'+str(bestError.get_value(1, 'T_EN')))
# print('3rd Best: '+str(bestError.get_value(2, 'HIDDEN'))+' hidden nodes. Sigma='
#     + str(bestError.get_value(2, 'SIGMA'))+' Seed='+str(bestError.get_value(2, 'SEED'))
#     + ' Training Error (T_EN):'+str(bestError.get_value(2, 'T_EN')))

# Now we know the configurations that have the lowest training error.
# Copy out a fresh NN, because VE's throw a tantrum and fall over itself
# after the first experiment.

beginTrain = input('Train against these architectures [y/n]?')

for currentIndex in range(0, bestArches.__len__()):
    cSig = str(bestArches.get_value(currentIndex, 'SIGMA'))
    cSeed = str(bestArches.get_value(currentIndex, 'SEED'))
    cHid = str(bestArches.get_value(currentIndex, 'HIDDEN'))

#   bbStr - what you pass to the NN executabless
#   fSufStr - folder nomeclature
    bbStr = cSeed+' '+cHid+' '+cSig
    fSufStr = cSeed+'-'+cHid+'-'+cSig

    archFolder = instancePath+'arch-'+fSufStr+'/'
    copyNewNetwork(archFolder)

    os.chdir(archFolder)
    #os.system('cp -Rfv '+instancePath+'in/* ./in/')
    os.system('cp '+instancePath+'test.dat ./')
    os.system('cp ./test.dat ./in/')
    os.system('cp ./in/test.dat ./in/TiAlloy_data-001n_input.dat')
    os.system('cp '+instancePath+'in/TiAlloy_data-001n_yield.dat ./in/')
    editCommittee(int(cHid), archFolder)
    os.system('./bin/no_of_lines.x ./in/test.dat ./in/no_of_rows.dat')
    # Process the inputs and output
    os.system('./bin/datapro -t -i'+'{:02d}'.format(numOfInputs)
                +' ./in/test.dat ./in/TiAlloy_data_001n_inputs_norm.dat')
    os.system('mv ./MINMAX ./MINMAX_inputs')

    os.system('./bin/datapro -t -i01 ./in/TiAlloy_data-001n_yield.dat ./in/TiAlloy_data-001n_yield_norm.dat')
    os.system('mv ./MINMAX ./MINMAX_outputs')
    os.system('cat ./MINMAX_inputs ./MINMAX_outputs > ./MINMAX')

    # Rewrite the MINMAX file, and copy it into the ./in/ directory.
    with open(archFolder+'/MINMAX', 'r') as minmaxFile:
        splitMinmax = minmaxFile.read().splitlines()

        fixedMinmax = []
        for idx, currentLine in enumerate(splitMinmax):
            if idx == numOfInputs+2:
                pass
            elif idx == numOfInputs+3:
                pass
            elif idx == numOfInputs+4:
                currentLine = currentLine.replace('1', str(numOfInputs+1), 1)
                fixedMinmax.append(currentLine)
            else:
                fixedMinmax.append(currentLine)
        minmaxFile.close()

    with open(archFolder+'/MINMAX', 'w') as minmaxFile:
        for currentLine in fixedMinmax:
            minmaxFile.writelines(currentLine+'\n')
        minmaxFile.close()

# Run the network
    os.chdir(archFolder)
    os.system('mv ./MINMAX ./in/MINMAX')
    os.system('./bin/normtest')
    replaceSpec(archFolder, numOfInputs, df_trainingInput.__len__(),
                (df_trainingInput.__len__() + df_testingInput.__len__()))
    os.system('./bin/bigback5 ./src/spec.t1 '+bbStr+' ./r/_R ./r/_r ./w/_sujoy')
    os.system('touch ./RESULT.DAT')
    os.system('./bin/generate44 ./src/spec.t1 '+cHid+' ./w/_sujoy ./w/_sujoy.lu')
    os.system('./bin/combine_com.x')
    os.system('./bin/gencom.x')
    os.system('./bin/treatout')

# Make some plots
    currentTrainedOutput = pd.read_csv(archFolder+'unnorm_com', sep=r'\s+')
    currentActualOutput = pd.read_csv(archFolder+'in/TiAlloy_data-001n_yield.dat', sep=r'\s+')
    plt.figure()
    plt.title('Output Comparison\n'
                +r'Arch: Seed: '+cSeed+r'   ${\sigma}$: '+cSig+'   Hidden: '+cHid)
    plt.ylabel('Output of Trained Network')
    plt.xlabel('Actual Output Values Used')
    plt.scatter(currentActualOutput, currentTrainedOutput.iloc[:,0])
    plt.savefig(archFolder+'OutputGraph.png')
    plt.close('all')

# Wait for the user to look at the graphs and decide which networks to do the Virtual 
# experiements from.
print('INFO: View the outputGraph.png files in the arch-* folders and decide')
print('INFO: which architectures to perform virtual experiments with.')
for idx in range(0, bestArches.__len__()):
    print('Arch '+str(idx)+': '+'Seed: '+str(bestArches.at[idx,'SEED'])
            +'\tHidden: '+str(bestArches.at[idx, 'HIDDEN'])+'\tSigma:'
            +str(bestArches.at[idx, 'SIGMA']))
print('INFO: Select architectures as a comma seperated list (ex: 1,3,7,6)')
VEArches = input('LIST: Select VE architectures:')
ArchList = VEArches.split(',')

for currentArch in ArchList:
    cSeed = str(bestArches.at[int(currentArch), 'SEED'])
    cHid = str(bestArches.at[int(currentArch), 'HIDDEN'])
    cSig = str(bestArches.at[int(currentArch), 'SIGMA'])

    fSufStr = cSeed+'-'+cHid+'-'+cSig
    archFolder = instancePath+'arch-'+fSufStr+'/'
    os.chdir(archFolder)

    minmax = pd.read_csv(archFolder+'in/MINMAX', sep=r'\s+')
    os.chdir(archFolder)
    os.system('mv ./unnorm_com ./originalResults')
    # Prepare the input files that will be used in the virtual experiments
    samplePoints = 100
    virtualExperimentInputs = []
    for currInput in range(0, numOfInputs):
        csvList = []
        for currColumn in range(0, numOfInputs):
                
                if currColumn == currInput:
                    csvList.append(np.linspace(minmax['MIN'][currColumn],
                                               minmax['MAX'][currColumn], samplePoints-1).tolist())
                else:
                    csvList.append([minmax['MEAN'][currColumn] for i in range(samplePoints-1)])
                
        virtualExperimentInputs.append(pd.DataFrame([csvList[i] for i in range(0, numOfInputs)]).transpose())

    # Write out the virtual experiment files using the following template:
    # ve_X(InputNumber).csv

    for i, currentVE in enumerate(virtualExperimentInputs):
        veFile = archFolder + '/ve_X' + str(i) + '.csv'
        currentVE.to_csv(veFile, sep=" ", index=False, index_label=False, header=False)
        os.chdir(archFolder)
        os.system('mv ./test.dat ./test.dat.original')
        os.system('rm ./test.dat')
        os.system('mv ./ve_X'+str(i)+'.csv ./test.dat')
        os.system('cp -f ./test.dat ./in/')
        os.system('cp -f ./test.dat ./in/TiAlloy_data-001n_input.dat')
        os.system('./bin/no_of_lines.x ./in/test.dat ./in/no_of_rows.dat')
        # os.system('./bin/datapro -i'+'{:02d}'.format(numOfInputs)
                    # +' ./in/test.dat ./in/TiAlloy_data-001n_inputs_norm.dat')
        os.system('rm unnorm_com com.dat RESULT.DAT')
        os.system('./bin/normtest')
        os.system('touch RESULT.DAT')

        os.system('./bin/generate44 ./src/spec.t1 '+cHid+' ./w/_sujoy ./w/_sujoy.lu')
        os.system('./bin/combine_com.x')
        os.system('./bin/gencom.x')
        os.system('./bin/treatout')
        os.system('mv ./unnorm_com ./VE-Result-'+str(i)+'.csv')
        
        plt.figure()
        plt.title('Virtual Experiment #'+str(i)+'\nArch: Seed: '+cSeed+r'  ${\sigma}$: '+cSig
                    +'  Hidden: '+str(cHid))
        plt.xlabel('Input Value')
        plt.ylabel('Network Response')
        VEInput = pd.read_csv(archFolder+'in/test.dat', sep=r'\s+',
                                names=['0', '1', '2', '3'])
        NNOutput = pd.read_csv(archFolder+'VE-Result-'+str(i)+'.csv', sep=r'\s+', 
                                names=['0', '1', '2', '3'])
        plt.scatter(VEInput.iloc[:,i], NNOutput.iloc[:,0])
        plt.savefig(archFolder+'VE-Result-Input-'+str(i+1)+'.png')
        plt.close('all')    
